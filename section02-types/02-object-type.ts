{
/* 16. OBJECT-TYPE */
/**
 * 16-1. 구체적이지 않은 object type 설정.
 * -타입지정 후 TS가 object type을 인식하지만
 * 정확한 특정정보가 없기 때문에 TS는 
 * 다른 어떤 type의 property도 취급하지 않음.
 * syntax) :object | :{}
 *  const notSpecificObj: object = {'k1':'v1'};
 *  const notSpecificObj: {} = {'k1':'v1'};
 */
const person: object = {
    name: 'aaaa',
    age: 3000
};
// console.log(person.name); //참조할 수 있는 특정정보가 없으므로 에러 발생함.

/**
 * 16-2. 구체적인 object type 설정
 * -특정 object type의 표기법을 설정하여
 * object 구조에 대한 정보를 제공
 * syntax) :{key1: type2; key2: type2; ... keyN: typeN;};
 *  const specificObj: {'k1': string} = {'k1':'v1'};
 */
const person2: { name: string; age: number; } = {
    name: 'aaaa',
    age: 3000
}
console.log(person2.name); //참조할 수 있는 특정정보가 있으므로 에러 발생하지 않음.

/**
 * 16-3. nested object type 설정
*/
const product: {
    id: string;
    price: number;
    tags: string[];
    details: {
      title: string;
      description: string;
    }
} = {
    id: 'abc1',
    price: 12.99,
    tags: ['great-offer', 'hot-and-new'],
    details: {
      title: 'Red Carpet',
      description: 'A great carpet - almost brand-new!'
    }
}

let unassignedObj = { 'k1': 'v1' };
let generalObj: {} = { 'k1': 'v1' };
let generalObj2: object = { 'k1': 'v1' };
let detailObj: {k1: string} = { 'k1': 'v1' };

let anyObjVar: any;
anyObjVar = unassignedObj;
anyObjVar = generalObj;
anyObjVar = generalObj2;
anyObjVar = detailObj;

let generalObjVar: {};
generalObjVar = unassignedObj;
generalObjVar = generalObj;
generalObjVar = generalObj2;
generalObjVar = detailObj;

let generalObj2Var: object;
generalObj2Var = unassignedObj;
generalObj2Var = generalObj;
generalObj2Var = generalObj2;
generalObj2Var = detailObj;

let detailObjVar: {k1: string};
detailObjVar = unassignedObj;
detailObjVar = generalObj;  //error: 'k1' 속성이 '{}' 형식에 없지만 '{ k1: string; }' 형식에서 필수입니다.
detailObjVar = generalObj2; //error: 'k1' 속성이 '{}' 형식에 없지만 '{ k1: string; }' 형식에서 필수입니다.
detailObjVar = detailObj;
}