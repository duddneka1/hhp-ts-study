<!-- 출처: https://fodor.org/blog/js-undefined-null-void/ -->

<!DOCTYPE html>
<html lang="en"><head>
	<meta charset="utf-8">
	<meta name="description" content='Dorin Fodor software engineer'>
	<meta name="keywords" content='dorin, fodor, software, engineer'>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href='https://fodor.org/css/bootstrap.min.css'>
	<title>JS: The difference between &#34;undefined&#34;, &#34;null&#34; and &#34;void 0&#34;</title>
</head>
<body>
    <div id="content">
    <section class="container text-monospace text-justified mt-3">
        <h2>JS: The difference between &#34;undefined&#34;, &#34;null&#34; and &#34;void 0&#34;</h2>
        <small class="text-secondary mb-5">Posted on January 17, 2020 | 3 minute read</small>
        <p><h3 id="heading">🍎🍊🍌</h3>
        <p>Why am I even asking this question, you might think. Well, the thing is that I have been asked this recently and I feel that I didn&rsquo;t give a good enough answer.</p>
        <p>Even though <code>undefined</code>, <code>null</code> and <code>void 0</code> have something in common, they cannot be compared directly because they represent different concepts with different functionalities.</p>
        <p>Instead of doing a one to one comparison between those, I think it makes more sense to explain what each of them is and by doing this, it will be clear how different they are.</p>
        <h3 id="undefined"><code>undefined</code></h3>
        <p>It is a <em>global property</em> or a <em>primitive value</em>.</p>
        <p>So, as you see, when you say &ldquo;undefined&rdquo; you could potentially be referring to two very different things.</p>
        <p>The global property named <code>undefined</code> has a value of <code>undefined</code> by default. This property could be modified up until ES5, when it was made read-only. Therefore if you try to change its value, you won&rsquo;t be able to:</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js"><span style="color:#66d9ef">undefined</span> <span style="color:#f92672">=</span> <span style="color:#ae81ff">1</span>

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">undefined</span>) <span style="color:#75715e">// undefined
        </span></code></pre></div><p>There is a way though to override the value of the global <code>undefined</code> property, even in the latest version of EcmaScript. This can be done by creating a scoped variable called <code>undefined</code> and giving it an arbitrary value. We are basically shadowing the built-in <code>undefined</code>.</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js">(<span style="color:#66d9ef">function</span>() {
        <span style="color:#66d9ef">var</span> <span style="color:#66d9ef">undefined</span> <span style="color:#f92672">=</span> <span style="color:#ae81ff">1</span>

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">undefined</span>) <span style="color:#75715e">// 1
        </span><span style="color:#75715e"></span>})()
        </code></pre></div><p>When it comes to the value of <code>undefined</code>, this is the default value for any variable that has been declared but not initialised.</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js"><span style="color:#66d9ef">var</span> <span style="color:#a6e22e">one</span>

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#a6e22e">one</span>) <span style="color:#75715e">// undefined
        </span></code></pre></div><p>Also, <code>undefined</code> is the value of an object&rsquo;s property that does not exist.</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js"><span style="color:#66d9ef">var</span> <span style="color:#a6e22e">obj</span> <span style="color:#f92672">=</span> {
        <span style="color:#a6e22e">hello</span><span style="color:#f92672">:</span> <span style="color:#e6db74">&#39;world&#39;</span>
        }

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#a6e22e">obj</span>.<span style="color:#a6e22e">goodbye</span>) <span style="color:#75715e">// undefined
        </span></code></pre></div><h3 id="null"><code>null</code></h3>
        <p>It is a <em>primitive value</em>.</p>
        <p>Similarly to the <code>undefined</code> primitive value it is also falsy, but it is not an identifier or a global property.</p>
        <p>Unlike <code>undefined</code>, it is not being assigned by default to anything in JavaScript. You can only manually set the value of <code>null</code>.</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js"><span style="color:#66d9ef">var</span> <span style="color:#a6e22e">nothing</span> <span style="color:#f92672">=</span> <span style="color:#66d9ef">null</span>

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#a6e22e">nothing</span>) <span style="color:#75715e">// null
        </span></code></pre></div><p>The common use case for <code>null</code> is to assign it to an identifier where an object can be expected but none is relevant.</p>
        <p>Because both <code>null</code> and <code>undefined</code> are falsy, when compared using the abstract comparison <code>==</code>, the result is going to be <code>true</code>. But, using the strict comparison <code>===</code>, the result is going to be <code>false</code>.</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js"><span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">null</span> <span style="color:#f92672">==</span> <span style="color:#66d9ef">undefined</span>) <span style="color:#75715e">// true
        </span><span style="color:#75715e"></span><span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">null</span> <span style="color:#f92672">===</span> <span style="color:#66d9ef">undefined</span>) <span style="color:#75715e">// false
        </span></code></pre></div><h3 id="void-expression"><code>void &lt;expression&gt;</code></h3>
        <p>It is an <em>operator</em>.</p>
        <p>Unlike both <code>undefined</code> and <code>null</code>, it does not represent a primitive value.</p>
        <p>The connection between <code>void</code> and the other two is that it always returns the value of <code>undefined</code>.</p>
        <p>Its purpose is to evaluate an expression (usually for its side effects) and then return <code>undefined</code>.</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js"><span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">void</span> <span style="color:#ae81ff">0</span>) <span style="color:#75715e">// undefined
        </span><span style="color:#75715e"></span><span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">void</span> (<span style="color:#ae81ff">1</span> <span style="color:#f92672">+</span> <span style="color:#ae81ff">1</span>)) <span style="color:#75715e">// undefined
        </span><span style="color:#75715e"></span><span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">void</span> (() =&gt; <span style="color:#ae81ff">5</span>)) <span style="color:#75715e">// undefined
        </span></code></pre></div><p>One other use for <code>void</code> is to retrieve the original value of <code>undefined</code> when the <code>undefined</code> identifier could have been overridden.</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js">(<span style="color:#66d9ef">function</span>() {
        <span style="color:#66d9ef">var</span> <span style="color:#66d9ef">undefined</span> <span style="color:#f92672">=</span> <span style="color:#ae81ff">1</span>

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">undefined</span>) <span style="color:#75715e">// 1
        </span><span style="color:#75715e"></span>
        <span style="color:#66d9ef">var</span> <span style="color:#a6e22e">realUndefined</span> <span style="color:#f92672">=</span> <span style="color:#66d9ef">void</span> <span style="color:#ae81ff">1</span>

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#a6e22e">realUndefined</span>) <span style="color:#75715e">// undefined
        </span><span style="color:#75715e"></span>})()
        </code></pre></div><p>But, as you remember the global property <code>undefined</code> is read-only, so we can retrieve its value without using <code>void</code>, like so:</p>
        <div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-js" data-lang="js">(<span style="color:#66d9ef">function</span>() {
        <span style="color:#66d9ef">var</span> <span style="color:#66d9ef">undefined</span> <span style="color:#f92672">=</span> <span style="color:#ae81ff">1</span>

        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#66d9ef">undefined</span>) <span style="color:#75715e">// 1
        </span><span style="color:#75715e"></span>
        <span style="color:#a6e22e">console</span>.<span style="color:#a6e22e">log</span>(<span style="color:#a6e22e">global</span>.<span style="color:#66d9ef">undefined</span>) <span style="color:#75715e">// undefined
        </span><span style="color:#75715e"></span>})()
        </code></pre></div><h3 id="conclusion">Conclusion</h3>
        <p>Quick recap:</p>
        <p><code>undefined</code> is a <em>global property</em> or a <em>primitive value</em></p>
        <p><code>null</code> is a <em>primitive value</em></p>
        <p><code>void &lt;expression&gt;</code> is an <em>operator</em></p>
        <p>As we have seen, we can find uses for all of them but only one of them is really indispensable: <code>undefined</code>.</p>
        <p>We can easily get by without <code>null</code> and especially <code>void</code> which seems to be an artifact of the past.</p>
        </p>
    </section>
    </div>
</body>
</html>