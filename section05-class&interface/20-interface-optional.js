"use strict";
{
    class Person {
        constructor(name) {
            this.prop = 'another-interface-prop';
            this.age = 30;
            if (name)
                this.name = name;
        }
        greet(phrase) {
            if (this.name) {
                console.log(phrase + '' + this.name);
            }
            else {
                console.log(phrase);
            }
        }
    }
    const noname = new Person();
}
