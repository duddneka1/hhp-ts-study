"use strict";
var _a, _b;
{
    // 01. use object literal
    const fetchUserData = {
        id: 'u1',
        name: 'asdf',
        // job: { title: 'ccc', desc: 'asdf'}
    };
    // console.log(fetchUserData.job.title);
    // at js
    console.log(fetchUserData.job && fetchUserData.job.title);
    // as ts over v3.7
    console.log((_a = fetchUserData === null || fetchUserData === void 0 ? void 0 : fetchUserData.job) === null || _a === void 0 ? void 0 : _a.title);
    // 02. use class
    class User {
        constructor(id, name) {
            this.id = id;
            this.name = name;
        }
    }
    const newUser = new User('aaa', 'bbb');
    // at js
    console.log(newUser.job && newUser.job.title);
    // as ts over v3.7
    console.log((_b = newUser === null || newUser === void 0 ? void 0 : newUser.job) === null || _b === void 0 ? void 0 : _b.title);
}
