"use strict";
{
    function add(a, b) {
        if (typeof a === 'string' || typeof b === 'string') {
            return a.toString() + b.toString();
        }
        return a + b;
    }
    // 함수표현식으로 사용할 떄는.. 안되나?
    const result1 = add(1, 5);
    // const result2 = add('ppp', 'aaa') as string;
    const result2 = add('ppp', 'aaa');
}
