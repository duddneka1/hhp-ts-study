"use strict";
{
    /**
     * ANY TYPE: Any kind of value, no specific type assignment
     * ts가 주는 모든 장점이 다 사라지고 vanilla js를 사용하는 것과 같다.
     * ts compiler는 아무것도 점검할 수 없다.
     * syntax) let Any: any;
     */
    let Any;
    let Object;
    let Array;
    let Tuple;
    let Union;
    let Function = function (p1) { };
}
