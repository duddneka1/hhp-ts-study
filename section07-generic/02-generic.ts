{
/**
 * built-in generic / generic
 */


// 01. Array Generic
// type Array = string[];
const notypename = ['a', 'b'];
const name: string[] = ['a', 'b'];
const nameG: Array<string> = ['a', 'b'];

notypename[0].split('');
name[0].split('');
nameG[0].split('');

// 02. Promise Generic
// const promise: Promise<unknown>
const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('This is done!');
    }, 1000)
});

// const promiseG: Promise<string>
const promiseG: Promise<string> = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('This is done!');
    }, 1000)
});

}