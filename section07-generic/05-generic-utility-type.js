"use strict";
{
    function createCourseGoal(title, description, date) {
        // let courseGoal: CourseGoal = {} ;
        let courseGoal = {};
        courseGoal.title = title;
        courseGoal.description = description;
        courseGoal.completeUntil = date;
        return courseGoal;
    }
    // 02. Readonly
    // 객체자체를 읽기전용 속성으로 처리한다.
    const names = ['ppp', 'aaa'];
    // names.push('mamam');
    // names.pop();
}
