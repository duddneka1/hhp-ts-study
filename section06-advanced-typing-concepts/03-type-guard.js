"use strict";
{
    const el = {
        name: 'ppororo',
        privileges: ['create-server'],
        startDate: new Date(),
    };
    // const universal: Universal = 'string';
    // 01. typeof를 사용하는 type-guard
    const add = function (a, b) {
        if (typeof a === 'string' || typeof b === 'string') {
            return a.toString() + b.toString();
        }
        return a + b;
    };
    const printEmployeeInfo = function (emp) {
        console.log(emp.name);
        // not use
        // if(typeof emp === 'object') {}
        if ('privileges' in emp) {
            console.log(emp.privileges);
        }
        if ('startDate' in emp) {
            console.log(emp.startDate);
        }
    };
    // 03. instanceof를 사용하는 type-guard
    class Car {
        drive() {
            console.log('Driving...');
        }
    }
    class Truck {
        drive() {
            console.log('Driving Truck...');
        }
        loadCargo(amount) { }
    }
    const v1 = new Car();
    const v2 = new Truck();
    const useVehicle = function (vehicle) {
        vehicle.drive();
        // if('loadCargo' in vehicle){
        //     vehicle.loadCargo(1000);
        // }
        if (vehicle instanceof Truck) {
            vehicle.loadCargo(1000);
        }
    };
    const moveAnimal = function (animal) {
        let speed;
        switch (animal.type) {
            case 'bird':
                speed = animal.flyingSpeed;
                break;
            case 'horse':
                speed = animal.runningSpeed;
        }
        console.log(speed);
    };
    moveAnimal({ type: 'bird', flyingSpeed: 1000 });
}
