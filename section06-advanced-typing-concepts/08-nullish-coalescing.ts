{
/**
 * nullish coalescing
 * keyword) ??
 * -undefined / null 일 경우에만 체크할때 사용하는 연산자
 */

// if value is empty string

// 01. use or opertator(||)
// return value is 'DEFAULT'
// because 0 or '' is recognized as a false value in javascript.
const useOR = '' || 'DEFAULT';

// 02. use nullish coalescing(??)
// return value is ''
// check only undefined or null
const useNULLISH = '' ?? 'DEFAULT';
}