{
/**
 * 01. generic function
 * 1. 함수 정의시 사용
 *   어떤 타입의 형태가 파라미터에 전달될지 정의하고 싶지 않을때 사용
 * 2. 함수 호출시 사용
 *   어떤 타입의 형태를 전달하는지 호출시 정확히 표현할 수 있음
 * 
 * 02. 제네릭타입에 타입제약을 사용
 * 
 * 03. keyof키워드를 이용한 제네릭타입에 타입제약(객체의 키다!)
*/

// 01-1. generic을 사용하지 않은 함수정의
// 1) 타입을 미리 지정하거나 
// >> 타입을 미리 지정하자면, 확실한 타입체크가 이뤄질 수 있겠지만 
// 항상 number라는 타입을 받아야하므로 범용성이 떨어진다.
const mergeT = (objA: object, objB: object): object => {
    return Object.assign(objA, objB);
}
// 2) any를 이용하여 구현할 수 있다.
//  >> 그렇다고 any를 사용한다면 자료의 타입을 제한할 수 없을 뿐더러
//  이 function을 통해 어떤 타입의 데이터가 리턴되는지 알 수 없다.
const mergeA = (objA: any, objB: any): any => {
    return Object.assign(objA, objB);
}

// 01-2. generic을 사용한 함수정의
// >> 이 함수는 T, U라는 형태의 변수 타입을 받고
// 인자로는 T, U형태를 받는다.
function mergeG<T, U>(objA: T, objB: U){
    return Object.assign(objA, objB);
}

const mergedObj = mergeG({name:'asdf'}, {age: 12});
const mergedObjG = mergeG<{name: string}, {age: number}>({name:'asdf'}, {age: 12});

}
{
// 02. 제네릭타입에 타입제약
// >> 이 함수는 어떤 형태도 괜찮은 object타입을 인자로 받는다. 라고 제약함.
function mergeG<T extends object, U extends object>(objA: T, objB: U){
    return Object.assign(objA, objB);
}

// 그냥 단순히 다른 예제임!
interface Lengthy {
    length: number;
}
function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
    let descriptonText = 'Got no value.';
    if(element.length  ===  1) {
        descriptonText = 'Got 1 elements';
    } else if(element.length  >  1) {
        descriptonText = 'Got' + element.length + ' elements';
    }
    
    return [element, descriptonText];
}
console.log(countAndDescribe('ppororororororo'));
}
{    
// 03. keyof키워드를 이용한 제네릭타입에 타입제약
function extractAndConvert<T extends object, U extends keyof T>(obj: T, key: U) {
    return obj[key];
}

}