{
/**
 * 01. ARRAY-TYPE
 * syntax) let Array: string[];
 */
const person = {
  name: 'asb',
  age: 30,
  //Array Type
  hobbies: ['sss', 'dddd']
}

//let favoriteActivies: any[];
let favoriteActivies: string[];
favoriteActivies = ['Sports'] ;
console.log(person.name);

for(const hobby of person.hobbies){
  console.log(hobby);
}
}