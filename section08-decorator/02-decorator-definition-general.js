"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
{
    // https://fe-churi.tistory.com/41
    // 데코레이터는 클래스 선언, 메서드, 접근자, 프로퍼티 또는 매개 변수에 첨부할 수 있는 특수한 종류의 선언입니다.
    // 데코레이터 함수에는 target(현재타겟), key(속성이름), descriptor(설명)가 전달됩니다.
    // 메소드나 클래스 인스턴스가 만들어지는 런타임에 실행됩니다. 즉, 매번 실행되지 않습니다.
    // 메서드, 접근자 또는 프로퍼티 데코레이터가 다음에 오는 매개 변수 데코레이터는 각 인스턴스 멤버에 적용됩니다.
    // 메서드, 접근자 또는 프로퍼티 데코레이터가 다음에 오는 매개 변수 데코레이터는 각 정적 멤버에 적용됩니다.
    // 매개 변수 데코레이터는 생성자에 적용됩니다.
    // 클래스 데코레이터는 클래스에 적용됩니다.
    // @:decorator-name
    // @:decorator-name() - use factory pattern
    function Logger(constructor) {
        console.log('Logging...');
        console.log(constructor);
    }
    let Person = class Person {
        constructor() {
            this.name = 'Max';
            console.log('create person');
        }
    };
    Person = __decorate([
        Logger
    ], Person);
    const pers = new Person();
}
