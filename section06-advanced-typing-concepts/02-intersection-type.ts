{
/**
 * 타입을 합침
 */
type Admin = {
    name: string;
    privileges: string[];
};

type Employee = {
    name: string;
    startDate: Date;
};

// interface ElevatedEmployee extends Admin, Employee {}
type ElevatedEmployee = Admin & Employee;

const el: ElevatedEmployee = {
    name: 'ppororo',
    privileges: ['create-server'],
    startDate: new Date(),    
}

type Combinable = string | number;
type Numeric = number | boolean;

type Universal = Combinable & Numeric;

// const universal: Universal = 'string';
}