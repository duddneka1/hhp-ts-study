{
/**
 * 02. TUPLE TYPE
 * 고정 길이와 및 각 인덱스가 고정타입을 갖는 배열(An array of fixed length and each index of a fixed type)
 * syntax) let Tuple: [number, string];
 * 
 */

let role: [number, string] = [1, 'admin'];
const person: {
  name: string;
  age: number;
  hobbies: string[];
  //Tuple Type
  role: [number, string];
} = {
  name: 'asdg',
  age: 141,
  hobbies: ['asdg'],
  role: [1, 'admin']
};
}