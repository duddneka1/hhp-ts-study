{
/**
 * 01. generic class
*/
class DataStorage {
    private data = [];

    addItem(item) {
        this.data.push(item);
    }

    removeITem(item) {
        this.data.splice(this.data.indexOf(item), 1);
    }

    getItems() {
        return [...this.data];
    }
}

class DataStorageG<T extends string | number | boolean> {
    private data: T[] = [];

    addItem(item: T) {
        this.data.push(item);
    }

    removeITem(item: T) {
        this.data.splice(this.data.indexOf(item), 1);
    }

    getItems() {
        return [...this.data];
    }
}
const textStorage = new DataStorageG<string>();
const numberStorage = new DataStorageG<number>();
// const objStorage = new DataStorageG<object>();

// using type
// >> 제네릭을 사용했을 때처럼 하나의 타입으로 고정되는 것이아니라
// 혼합된 배열을 갖게되어버린다!!!!!!
type Primitive = string | number | boolean
class DataStorageT {
    private data: Primitive[] = [];

    addItem(item: Primitive) {
        this.data.push(item);
    }

    removeITem(item: Primitive) {
        this.data.splice(this.data.indexOf(item), 1);
    }

    getItems() {
        return [...this.data];
    }
}
const textStorageT   = new DataStorageT();
const numberStorageT = new DataStorageT();
const objStorageT    = new DataStorageT();


// JS CODE
// var __spreadArray = (this && this.__spreadArray) || funcvar __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
//     if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
//         if (ar || !(i in from)) {
//             if (!ar) ar = Array.prototype.slice.call(from, 0, i);
//             ar[i] = from[i];
//         }
//     }
//     return to.concat(ar || Array.prototype.slice.call(from));
// };
// var DataStorageG = /** @class */ (function () {
//     function DataStorageG() {
//         this.data = [];
//     }
//     DataStorageG.prototype.addItem = function (item) {
//         this.data.push(item);
//     };
//     DataStorageG.prototype.removeITem = function (item) {
//         this.data.splice(this.data.indexOf(item), 1);
//     };
//     DataStorageG.prototype.getItems = function () {
//         return __spreadArray([], this.data, true);
//     };
//     return DataStorageG;
// }());
// var textStorage = new DataStorageG();
// var numberStorage = new DataStorageG();

}