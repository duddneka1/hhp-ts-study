{

// 새로운 생성자를 반환하는 데코레이터
// 정의되던 때 실행되던 로직을
// 인스턴스 활 될때 실행되게 변경할 수 있다.
function withTemplate(template: string, hookId: string) {
    return function<T extends { new(...args: any[]): {name: string} }>(originalConstructor: T) {
        return class extends originalConstructor {  
            constructor(..._: any[]){
                super();
                const hookEl = document.getElementById(hookId);
                if(hookEl) {
                    hookEl.innerHTML = template;
                    hookEl.querySelector('h1')!.textContent = this.name;
                }
            }
        }
    }
}

function Logger(constructor: Function) {
    console.log('Logging...');
    console.log(constructor);
}

// 데코레이터는 여러개를 추가할 수 있다
// 실행순서는 아래에서 위로 실행된다 bottom-up
@Logger
@withTemplate('<h1>My Person Object</h1>', 'app')
class Person {
    name = 'Max';
    constructor(){
        console.log('create person');
    }
}
// const ppp = new Person();
}