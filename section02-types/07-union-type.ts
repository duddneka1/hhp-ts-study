{   
    /**
     * UNION TYPE: 두종류 이상의 타입이 사용가능 할 때 사용한다
     * operator: '|' 
     * let strOrNum: string | number;
     */
    const combine = function(i1: string | number, i2: string | number){
        // i1과 i2의 타입을 유니온타입을 통해 지정하였지만 타입스크립트는
        // '+' 연산자가 사용되지 않는 타입이 올수도 판단하여 에러를 제공한다.
        // (=정확히 어떤 타입이 오는지는 runtime에서 확인할 수 있는 사항이기 때문에)
        // error message: '+' 연산자를 'string | number' 및 'string | number' 형식에 적용할 수 없습니다.
        // const result = i1 + i2; 

        // 어떤 로직을 처리하느냐에 따라 
        // 부가적인 런타임 타입체크(extra runtime type check) 여부가 갈림
        let result;
        if(typeof i1 === 'number' && i2 === 'number'){
            result = i1 + i2;
        } else { 
            result = i1.toString().concat(i2.toString());
        }

        return result;
    }

    const numberCombine = combine(30, 26);
    const stringCombine = combine('A', 'B');
}