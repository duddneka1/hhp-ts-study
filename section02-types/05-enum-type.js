"use strict";
{
    /**
     * 03. ENUM TYPE: Automatically enumerated global constant identifiers
     * 글로벌 상수를 숫자로 나타내며, 사람이 읽을 수 있는 레이블을 할당
     * 따라서, 사람이 읽을 수 있는 식별자를 구성할 때 매우 유용합니다.
     * syntax) enum EnumName {NEW, OLD};
     */
    /* PRESENTATE FOR TS */
    /* NUMBER | STRING을 갖습니다. */
    let DefaultEnum;
    (function (DefaultEnum) {
        DefaultEnum[DefaultEnum["A"] = 0] = "A";
        DefaultEnum[DefaultEnum["B"] = 1] = "B";
        DefaultEnum[DefaultEnum["C"] = 2] = "C";
    })(DefaultEnum || (DefaultEnum = {}));
    ;
    let HaveValueEnum;
    (function (HaveValueEnum) {
        HaveValueEnum[HaveValueEnum["A"] = 5] = "A";
        HaveValueEnum[HaveValueEnum["B"] = 100] = "B";
        HaveValueEnum[HaveValueEnum["C"] = 200] = "C";
    })(HaveValueEnum || (HaveValueEnum = {}));
    ;
    let ComplexValueEnum;
    (function (ComplexValueEnum) {
        ComplexValueEnum[ComplexValueEnum["A"] = 0] = "A";
        ComplexValueEnum[ComplexValueEnum["B"] = 0] = "B";
        ComplexValueEnum["C"] = "C";
    })(ComplexValueEnum || (ComplexValueEnum = {}));
    ;
    /**
     * Boolean, Object, Function 할당 가능여부 확인.
     * 결론: 안됩니다. 다음과 같은 에러를 발생시킵니다.
     **/
    /*
    enum bool { A = true }; //숫자 열거형에는 컴퓨팅된 구성원만 사용할 수 있는데 이 식에는 'true' 형식이 있습니다
    enum R { A = function(){} }; //계산된 값은 문자열 값 멤버가 포함된 열거형에서 허용되지 않습니다.
    enum R { A = {} }; //계산된 값은 문자열 값 멤버가 포함된 열거형에서 허용되지 않습니다.
    */
    // USING-EX) Declare And Assign
    // 1. Declare
    let Role;
    (function (Role) {
        Role[Role["ADMIN"] = 0] = "ADMIN";
        Role[Role["READ_ONLY"] = 1] = "READ_ONLY";
        Role[Role["AUTHOR"] = 2] = "AUTHOR";
        Role[Role["PPORORO"] = 3] = "PPORORO";
    })(Role || (Role = {}));
    ;
    const person = {
        name: 'asdg',
        age: 141,
        hobbies: ['asdg'],
        // 2. Assign
        role: Role.ADMIN
    };
    /* full syntax using ts */
    const ppororo = {
        name: 'ppororo',
        age: 7,
        favorite: 'play',
        hobbies: ['swim', 'game'],
        role: Role.PPORORO
    };
    /* PRESENTATE FOR JS */
    /* GENERAL JS CODE */
    // 1. xx code.
    const ADMIN = 0;
    const READ_ONLY = 1;
    const AUTHOR = 2;
    const PPORORO = 'YEAH!';
    // 2. better than above code but it is xx code too.
    const XXCode = {
        ADMIN: 0,
        READ_ONLY: 1,
        AUTHOR: 2,
        PPORORO: 'YEAH!'
    };
    /* MORE HIGH-LEVEL JS CODE */
    //1. before es6
    var BeforeRole = void 0; //WHAT IS VOID?! GO AND READ FILE '101-etc-diff-void0-undefined-null.html'
    (function (BeforeRole) {
        BeforeRole[BeforeRole["ADMIN"] = 0] = "ADMIN";
        BeforeRole[BeforeRole["READ_ONLY"] = 1] = "READ_ONLY";
        BeforeRole[BeforeRole["AUTHOR"] = 2] = "AUTHOR";
        BeforeRole[BeforeRole["PPORORO"] = 'YEAH!'] = "PPORORO";
    })(BeforeRole || (BeforeRole = {}));
    ;
    //2. since es6
    let AfterRole;
    (function (AfterRole) {
        AfterRole[AfterRole["ADMIN"] = 0] = "ADMIN";
        AfterRole[AfterRole["READ_ONLY"] = 1] = "READ_ONLY";
        AfterRole[AfterRole["AUTHOR"] = 2] = "AUTHOR";
        AfterRole[AfterRole["PPORORO"] = 'YEAH!'] = "PPORORO";
    })(AfterRole || (AfterRole = {}));
    ;
}
